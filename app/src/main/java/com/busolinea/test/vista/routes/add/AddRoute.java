package com.busolinea.test.vista.routes.add;

import android.content.Context;

public interface AddRoute {

    interface View{
        void updateID(String id);
        void onError(String msg);
        void addFinished();
    }

    interface Presenter{
        void updateID(String id);
        void onError(String msg);
        void addFinished();

        void addRoute(String id, String origin, String destination, String departure, String arrival, String company, Context context);
        void getID();
    }

    interface Model{
        void addRoute(String id, String origin, String destination, String departure, String arrival, String company, Context context);
        void getID();
    }

}
