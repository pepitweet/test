package com.busolinea.test.vista.login;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.busolinea.test.R;
import com.busolinea.test.vista.routes.RoutesView;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

public class LoginView extends AppCompatActivity implements Login.View {
    private Login.Presenter presenter;

    TextInputEditText txtUser;
    TextInputEditText txtPass;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Autenticación");
        presenter = new LoginPresenter(this);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        initControls();
    }

    private void initControls(){
        txtUser = findViewById(R.id.txtUsuario);
        txtPass = findViewById(R.id.txtPass);
    }
    
    public void btnAutenticarClick(View v){
        presenter.autenticate(this, txtUser.getText().toString(), txtPass.getText().toString());
    }
    
    public void btnContinuarClick(View v){
        Intent i = new Intent(this, RoutesView.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onError(String msgError) {
        final PrettyDialog pDialog = new PrettyDialog(this);
        pDialog.setTitle("Error");
        pDialog.setMessage(msgError);
        pDialog.setIcon(R.drawable.pdlg_icon_info, R.color.pdlg_color_red, new PrettyDialogCallback() {
            @Override
            public void onClick() {
                pDialog.dismiss();
            }
        });
        pDialog.show();
    }

    @Override
    public void autenticated() {
        Intent i = new Intent(this, RoutesView.class);
        startActivity(i);
    }
}
