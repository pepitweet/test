package com.busolinea.test.vista.routes;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.busolinea.test.common.pojo.getRoutes.Data;
import com.busolinea.test.recyclerview.RoutesAdapter;
import com.busolinea.test.restClient.RestClient;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.MODE_PRIVATE;

public class RoutesModel implements Routes.Model {
    private Routes.Presenter presenter;
    public static final String MY_PREFS_NAME = "UserAutenticate";

    public RoutesModel(Routes.Presenter presenter){
        this.presenter = presenter;
    }

    @Override
    public void updateRoutes(final Context context) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();

        Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl("https://0sysjslkra.execute-api.us-east-1.amazonaws.com/test/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RestClient restClient = retrofit.create(RestClient.class);

        Call<com.busolinea.test.common.pojo.getRoutes.Routes> call = restClient.getData();

        call.enqueue(new Callback<com.busolinea.test.common.pojo.getRoutes.Routes>() {
            @Override
            public void onResponse(Call<com.busolinea.test.common.pojo.getRoutes.Routes> call, Response<com.busolinea.test.common.pojo.getRoutes.Routes> response) {
                if(response.code() == 200){
                    try {
                        Data data = response.body().getData();
                        RoutesAdapter adapter = new RoutesAdapter(data.getRoutes(), context);
                        presenter.onUpdateData(adapter);
                    }catch (Exception e){
                        presenter.onError("Error inesperado al procesar la solicitud");
                    }
                }else{
                    presenter.onError("Respuesta incorrecta CODE: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<com.busolinea.test.common.pojo.getRoutes.Routes> call, Throwable t) {
                presenter.onError("Error en la respuesta del servidor");
            }
        });
    }

    @Override
    public void deleteRoute(final Context context, String id) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl("https://0sysjslkra.execute-api.us-east-1.amazonaws.com/test/")
                .client(okHttpClient)
                .build();

        RestClient restClient = retrofit.create(RestClient.class);

        Call<ResponseBody> call = restClient.deleteData(id);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    try {
                        if (response.isSuccessful()) {
                            updateRoutes(context);
                        } else {
                            presenter.onError("Error al procesar la solicitud. " + response.errorBody());
                        }
                    }catch (Exception e){
                        presenter.onError("Error inesperado al procesar la solicitud");
                    }
                }else{
                    presenter.onError("Error al procesar la solicitud. CODE: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                presenter.onError("Error al procesar la solicitud");
            }
        });
    }

    @Override
    public void getPermisos(Context context) {
        SharedPreferences pref = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String userName = pref.getString("user_name", null);
        String userPass = pref.getString("user_pass", null);
        if(userName != null && userPass != null) {
            presenter.getSessionStatus(true);
        }
    }
}
