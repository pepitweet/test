package com.busolinea.test.vista;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.busolinea.test.R;
import com.busolinea.test.vista.login.LoginView;
import com.busolinea.test.vista.routes.RoutesView;

public class SplashActivity extends AppCompatActivity {
    public static final String MY_PREFS_NAME = "UserAutenticate";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        SharedPreferences pref = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String userName = pref.getString("user_name", null);
        String userPass = pref.getString("user_pass", null);
        if(userName == null || userPass == null) {
            Intent i = new Intent(this, LoginView.class);
            startActivity(i);
        }else{
            Intent i = new Intent(this, RoutesView.class);
            startActivity(i);
        }
        finish();
    }
}
