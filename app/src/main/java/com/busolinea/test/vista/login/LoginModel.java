package com.busolinea.test.vista.login;

import android.content.Context;
import android.content.SharedPreferences;

import com.busolinea.test.common.Validators;

import static android.content.Context.MODE_PRIVATE;

public class LoginModel implements Login.Model {
    private Login.Presenter presenter;

    public static final String MY_PREFS_NAME = "UserAutenticate";

    public LoginModel(Login.Presenter presenter){
        this.presenter = presenter;
    }


    @Override
    public void autenticate(Context context, String user, String pass) {
        String msgError = "";
        if(user != null && pass != null){
            if(!Validators.getValidString(user).equals("") && Validators.validatePass(pass)){
                SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString("user_name", user);
                editor.putString("user_pass", pass);
                editor.apply();
            }else{
                msgError = "Usuario o contraseña inválidos";
            }
        }else{
            msgError = "No se recibió la información del usuario";
        }
        if(msgError.equals("")){
            presenter.autenticated();
        }else{
            presenter.onError(msgError);
        }
    }
}
