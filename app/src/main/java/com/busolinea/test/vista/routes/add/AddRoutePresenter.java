package com.busolinea.test.vista.routes.add;

import android.content.Context;

public class AddRoutePresenter implements AddRoute.Presenter {
    private AddRoute.View view;
    private AddRoute.Model model;

    public AddRoutePresenter(AddRoute.View view){
        this.view = view;
        model = new AddRouteModel(this);
    }

    @Override
    public void updateID(String id) {
        if(view != null)
            view.updateID(id);
    }

    @Override
    public void addFinished() {
        if(view != null)
            view.addFinished();
    }

    @Override
    public void onError(String msg) {
        if(view != null)
            view.onError(msg);
    }

    @Override
    public void getID() {
        if(model != null)
            model.getID();
    }

    @Override
    public void addRoute(String id, String origin, String destination, String departure, String arrival, String company, Context context) {
        if(model != null)
            model.addRoute(id, origin, destination, departure, arrival, company, context);
    }
}
