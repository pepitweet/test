package com.busolinea.test.vista.routes;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.busolinea.test.R;
import com.busolinea.test.common.pojo.getRoutes.Route;
import com.busolinea.test.recyclerview.RoutesAdapter;
import com.busolinea.test.vista.routes.add.AddRouteView;
import com.hussain_chachuliya.gifdialog.GifDialog;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

public class RoutesView extends AppCompatActivity implements Routes.View {
    private Routes.Presenter presenter;
    boolean autenticated = false;

    private GifDialog gDialog;
    PrettyDialog pDialog;
    private final static String TAG_LOAD_DIALOG = "LOAG_DIALOG";

    RecyclerView reciclador;
    Button btnAgregar;

    int rowsGrid = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routes);
        setTitle("Corridas");
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            rowsGrid = 1;
        }
        initContols();
        presenter = new RoutesPresenter(this);
        presenter.getPermisos(this);
    }

    private void initContols(){
        btnAgregar = findViewById(R.id.btnAgregar);
        reciclador = findViewById(R.id.reciclador);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showLoadDialog();
        presenter.updateRoutes(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(pDialog != null)
            if(pDialog.isShowing())
                pDialog.dismiss();
    }

    @Override
    public void onUpdateData(RoutesAdapter adapter) {
        dismissLoadDialog();
        reciclador.setLayoutManager(new GridLayoutManager(this, rowsGrid));
        reciclador.setAdapter(adapter);
    }

    @Override
    public void onError(String msg) {
        dismissLoadDialog();
        pDialog = new PrettyDialog(this);
        pDialog.setTitle("Error");
        pDialog.setMessage(msg);
        pDialog.setIcon(R.drawable.pdlg_icon_info, R.color.pdlg_color_red, new PrettyDialogCallback() {
            @Override
            public void onClick() {
                pDialog.dismiss();
            }
        });
        pDialog.show();
    }

    @Override
    public void getSessionStatus(boolean autenticated) {
        this.autenticated =autenticated;
    }

    public void btnAgregarClick(View v){
        if(autenticated){
            Intent i = new Intent(this, AddRouteView.class);
            startActivity(i);
        }else{
            Snackbar.make(reciclador, "Usuario no autenticado", Snackbar.LENGTH_SHORT).show();
        }
    }

    public void itemSelecion(final Route route){
        if(autenticated){
            pDialog = new PrettyDialog(this);
            pDialog.setTitle("Eliminar");
            pDialog.setMessage(getString(R.string.confirmation_delete_route, route.getId()));
            pDialog.setIcon(R.drawable.pdlg_icon_info, R.color.colorAccent, new PrettyDialogCallback() {
                @Override
                public void onClick() {
                    pDialog.dismiss();
                }
            });
            pDialog.addButton("Aceptar", R.color.pdlg_color_white, R.color.colorAccent, new PrettyDialogCallback() {
                @Override
                public void onClick() {
                    pDialog.dismiss();
                    showLoadDialog();
                    presenter.deleteRoute(RoutesView.this, route.getId());
                }
            });
            pDialog.addButton("Cancelar", R.color.pdlg_color_white, R.color.colorPrimaryDark, new PrettyDialogCallback() {
                @Override
                public void onClick() {
                    pDialog.dismiss();
                }
            });
            pDialog.show();
        }
    }

    private void showLoadDialog(){
        if(gDialog == null){
            gDialog = GifDialog
                    .Companion
                    .with(this)
                    .setHeight(250)
                    .setWidth(250)
                    .isCancelable(false)
                    .setText("Cargando...")
                    .setTextSize(18)
                    .setTextBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
                    .setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
                    .setResourceId(R.drawable.gif_load);
            gDialog.showDialog(TAG_LOAD_DIALOG);
        }
    }

    private void dismissLoadDialog(){
        if(gDialog != null) {
            gDialog.dismissDialog(TAG_LOAD_DIALOG);
            gDialog = null;
        }
    }
}
