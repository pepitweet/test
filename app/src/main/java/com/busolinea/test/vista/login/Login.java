package com.busolinea.test.vista.login;

import android.content.Context;

public interface Login {

    interface View{
        void onError(String msgError);
        void autenticated();
    }

    interface Presenter{
        void onError(String msgError);
        void autenticated();

        void autenticate(Context context, String user, String pass);
    }

    interface Model{
        void autenticate(Context context, String user, String pass);
    }
}
