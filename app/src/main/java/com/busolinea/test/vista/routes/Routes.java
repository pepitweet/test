package com.busolinea.test.vista.routes;

import android.content.Context;

import com.busolinea.test.recyclerview.RoutesAdapter;

public interface Routes {

    interface View{
        void onError(String msg);
        void getSessionStatus(boolean autenticated);
        void onUpdateData(RoutesAdapter adapter);
    }

    interface Presenter{
        void onUpdateData(RoutesAdapter adapter);
        void getSessionStatus(boolean autenticated);
        void onError(String msg);

        void deleteRoute(Context context, String id);
        void updateRoutes(Context context);
        void getPermisos(Context context);
    }

    interface Model{
        void deleteRoute(Context context, String id);
        void updateRoutes(Context context);
        void getPermisos(Context context);
    }

}
