package com.busolinea.test.vista.routes.add;

import android.app.TimePickerDialog;
import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TimePicker;

import com.busolinea.test.R;
import com.hussain_chachuliya.gifdialog.GifDialog;

import java.util.Calendar;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

public class AddRouteView extends AppCompatActivity implements AddRoute.View {
    private AddRoute.Presenter presenter;

    private GifDialog gDialog;
    private final static String TAG_LOAD_DIALOG = "LOAG_DIALOG";

    private static final String CERO = "0";
    private static final String DOS_PUNTOS = ":";
    public final Calendar c = Calendar.getInstance();
    final int hora = c.get(Calendar.HOUR_OF_DAY);
    final int minuto = c.get(Calendar.MINUTE);

    TextInputEditText txtID;
    TextInputEditText txtOrigen;
    TextInputEditText txtDestino;
    TextInputEditText txtSalida;
    TextInputEditText txtLlegada;
    TextInputEditText txtMarca;

    InputMethodManager imm = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_route);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Nueva ruta");
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        presenter = new AddRoutePresenter(this);
        initControls();
        initEvents();
        presenter.getID();
    }

    private void initControls(){
        txtID = findViewById(R.id.txtID);
        txtID.setKeyListener(null);
        txtOrigen = findViewById(R.id.txtOrigen);
        txtDestino = findViewById(R.id.txtDestino);
        txtSalida = findViewById(R.id.txtSalida);
        txtSalida.setKeyListener(null);
        txtLlegada = findViewById(R.id.txtLlegada);
        txtLlegada.setKeyListener(null);
        txtMarca = findViewById(R.id.txtMarca);
    }

    private void initEvents(){
        txtSalida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtSalida.setText("");
                imm.hideSoftInputFromWindow(txtSalida.getWindowToken(), 0);
                obtenerHora(txtSalida);
            }
        });

        txtLlegada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtLlegada.setText("");
                imm.hideSoftInputFromWindow(txtLlegada.getWindowToken(), 0);
                obtenerHora(txtLlegada);
            }
        });
    }

    private void obtenerHora(final TextInputEditText cntrl){
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
                String AM_PM;
                if(hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }
                cntrl.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
            }
        }, hora, minuto, false);
        recogerHora.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_add_route, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            case R.id.guardar:
                showLoadDialog();
                presenter.addRoute(
                        txtID.getText().toString(),
                        txtOrigen.getText().toString(),
                        txtDestino.getText().toString(),
                        txtSalida.getText().toString(),
                        txtLlegada.getText().toString(),
                        txtMarca.getText().toString(),
                        AddRouteView.this);
                break;
        }
        return true;
    }

    @Override
    public void updateID(String id) {
        txtID.setText(id);
    }

    @Override
    public void onError(String msg) {
        dismissLoadDialog();
        final PrettyDialog pDialog = new PrettyDialog(this);
        pDialog.setTitle("Error");
        pDialog.setMessage(msg);
        pDialog.setIcon(R.drawable.pdlg_icon_info, R.color.pdlg_color_red, new PrettyDialogCallback() {
            @Override
            public void onClick() {
                pDialog.dismiss();
            }
        });
        pDialog.show();
    }

    @Override
    public void addFinished() {
        dismissLoadDialog();
        final PrettyDialog pDialog = new PrettyDialog(this);
        pDialog.setTitle("Exito");
        pDialog.setMessage("Registro agregado exitosamente!");
        pDialog.setIcon(R.drawable.pdlg_icon_info);
        pDialog.addButton("Aceptar", R.color.pdlg_color_white, R.color.colorAccent, new PrettyDialogCallback() {
            @Override
            public void onClick() {
                pDialog.dismiss();
                finish();
            }
        });
        pDialog.show();
    }

    private void showLoadDialog(){
        if(gDialog == null){
            gDialog = GifDialog
                    .Companion
                    .with(this)
                    .setHeight(250)
                    .setWidth(250)
                    .isCancelable(false)
                    .setText("Cargando...")
                    .setTextSize(18)
                    .setTextBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
                    .setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
                    .setResourceId(R.drawable.gif_load);
            gDialog.showDialog(TAG_LOAD_DIALOG);
        }
    }

    private void dismissLoadDialog(){
        if(gDialog != null) {
            gDialog.dismissDialog(TAG_LOAD_DIALOG);
            gDialog = null;
        }
    }

}
