package com.busolinea.test.vista.routes.add;

import android.content.Context;

import com.busolinea.test.common.DateManage;
import com.busolinea.test.common.pojo.getRoutes.Route;
import com.busolinea.test.restClient.RestClient;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddRouteModel implements AddRoute.Model {
    private AddRoute.Presenter presenter;

    public AddRouteModel(AddRoute.Presenter presenter){
        this.presenter = presenter;
    }

    @Override
    public void getID() {
        presenter.updateID(DateManage.getDateHourString());
    }

    @Override
    public void addRoute(String id, String origin, String destination, String departure, String arrival, String company, Context context) {
        String msgError = "";
        if(id.trim().equals(""))
            msgError += "\n* ID inválido.";
        if(origin.trim().equals(""))
            msgError += "\n* Ingrese el origen.";
        if(destination.trim().equals(""))
            msgError += "\n* Ingrese el destino.";
        if(departure.trim().equals(""))
            msgError += "\n* Seleccione la hora de salida.";
        if(arrival.trim().equals(""))
            msgError += "\n* Seleccione la hora de llegada.";
        if(company.trim().equals(""))
            msgError += "\n* Ingrese la marca.";

        if(msgError.trim().equals("")){
            Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit
                    .Builder()
                    .baseUrl("https://0sysjslkra.execute-api.us-east-1.amazonaws.com/test/")
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            RestClient restClient = retrofit.create(RestClient.class);

            Route route = new Route();
            route.setId(id);
            route.setOriginId(origin);
            route.setDestinationId(destination);
            route.setDepartureTime(departure);
            route.setArrivalTime(arrival);
            route.setCompanyName(company);

            Call<ResponseBody> call = restClient.postData(route);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.code() == 200){
                        if(response.isSuccessful()){
                            presenter.addFinished();
                        }else{
                            presenter.onError("Error al procesar la solicitud. " + response.errorBody());
                        }
                    }else{
                        presenter.onError("Error al procesar la solicitud. CODE: " + response.code());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    presenter.onError("Error al procesar la solicitud");
                }
            });

        }else{
            presenter.onError(msgError.trim());
        }
    }

}
