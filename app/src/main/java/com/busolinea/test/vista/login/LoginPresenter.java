package com.busolinea.test.vista.login;

import android.content.Context;

public class LoginPresenter implements Login.Presenter {

    private Login.View view;
    private Login.Model model;

    public LoginPresenter(Login.View view){
        this.view = view;
        model = new LoginModel(this);
    }

    @Override
    public void autenticate(Context context, String user, String pass) {
        if(model != null)
            model.autenticate(context, user, pass);
    }

    @Override
    public void autenticated() {
        if(view != null)
            view.autenticated();
    }

    @Override
    public void onError(String msgError) {
        if(view != null)
            view.onError(msgError);
    }
}
