package com.busolinea.test.vista.routes;

import android.content.Context;

import com.busolinea.test.recyclerview.RoutesAdapter;

public class RoutesPresenter implements Routes.Presenter {
    private Routes.Model model;
    private Routes.View view;

    public RoutesPresenter(Routes.View view){
        this.view = view;
        model = new RoutesModel(this);
    }

    @Override
    public void getPermisos(Context context) {
        if(model != null)
            model.getPermisos(context);
    }

    @Override
    public void updateRoutes(Context context) {
        if(model != null)
            model.updateRoutes(context);
    }

    @Override
    public void onUpdateData(RoutesAdapter adapter) {
        if(view != null)
            view.onUpdateData(adapter);
    }

    @Override
    public void onError(String msg) {
        if(view != null)
            view.onError(msg);
    }

    @Override
    public void getSessionStatus(boolean autenticated) {
        if(view != null)
            view.getSessionStatus(autenticated);
    }

    @Override
    public void deleteRoute(Context context, String id) {
        if(model != null)
            model.deleteRoute(context, id);
    }
}
