package com.busolinea.test.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateManage {

    /**
     * Método para expresar en String la fecha y hora actual incluyendo hasta milisegundos
     * La cadena retornada tiene el formato: yyyyMMddHHmmssSSS
     * @return String con la fecha y hora con el formato yyyyMMddHHmmssSSS
     */
    public static String getDateHourString(){
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String formatTimeToStringMex(Date dateTime){
        if (dateTime==null)
            return null;

        java.text.SimpleDateFormat sdf =  new java.text.SimpleDateFormat("HH:mm:ss");

        return sdf.format(dateTime);
    }

    /**
     * Convierte una cadena en formato DateTime SQL (dd/MM/yyyy) a Date de JAVA
     * @param dateTimeStr
     * @return
     */
    public static Date parseDateMex(String dateTimeStr){
        if (dateTimeStr==null)
            return null;
        if (dateTimeStr.trim().equals(""))
            return null;

        Date response = null;
        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            response = dateFormat.parse(dateTimeStr);
        }catch(Exception ex){
            ex.printStackTrace();
        }

        return response;
    }

}
