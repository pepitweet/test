package com.busolinea.test.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validators {

    private final static String REGEX_PASS = "^(?=.*[0-9])(?=.*[a-zA-Z])(?=\\S+$).{6,}$";

    public static boolean validatePass(String pass){
        Pattern p = Pattern.compile(REGEX_PASS);
        Matcher m = p.matcher(pass);
        return m.matches();
    }

    public static String getValidString(String value){
        String validString =  (value!=null?value.trim():"");
        if(validString.trim().toUpperCase().equals("NULL"))
            validString = "";
        return validString;

    }

}
