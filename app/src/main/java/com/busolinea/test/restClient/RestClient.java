package com.busolinea.test.restClient;

import com.busolinea.test.common.pojo.getRoutes.Data;
import com.busolinea.test.common.pojo.getRoutes.Route;
import com.busolinea.test.common.pojo.getRoutes.Routes;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RestClient {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
            "x-api-key: Yrk3AZ1yOT7PIBbWJNrkB541cLBnff5w6cSZH9qr"
    })
    @GET("routes")
    Call<Routes> getData();

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
            "x-api-key: Yrk3AZ1yOT7PIBbWJNrkB541cLBnff5w6cSZH9qr"
    })
    @DELETE("routes/{id}")
    Call<ResponseBody> deleteData(@Path("id") String id);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
            "x-api-key: Yrk3AZ1yOT7PIBbWJNrkB541cLBnff5w6cSZH9qr"
    })
    @POST("routes")
    Call<ResponseBody> postData(@Body Route body);

}
