package com.busolinea.test.recyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.busolinea.test.R;
import com.busolinea.test.common.pojo.getRoutes.Route;
import com.busolinea.test.vista.routes.RoutesView;

import java.util.List;

public class RoutesAdapter extends RecyclerView.Adapter<RoutesAdapter.RoutesAdapterViewHolder> {

    private List<Route> data;
    private Context adapterContext;

    public RoutesAdapter(List<Route> data, Context adapterContext){
        this.data = data;
        this.adapterContext = adapterContext;
    }

    @NonNull
    @Override
    public RoutesAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new RoutesAdapterViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_item_ruta, viewGroup, false));
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if(data != null)
            count = data.size();
        return count;
    }

    @Override
    public void onBindViewHolder(@NonNull RoutesAdapterViewHolder holder, final int position) {
        Route route = data.get(position);
        holder.lblID.setText(route.getId());
        holder.lblMarca.setText(route.getCompanyName());
        holder.lblOrigen.setText(adapterContext.getString(R.string.origen_label, route.getOriginId()));
        holder.lblDestino.setText(adapterContext.getString(R.string.destino_label, route.getDestinationId()));
        holder.lblSalida.setText(adapterContext.getString(R.string.salida_label, route.getDepartureTime()));
        holder.lblLlegada.setText(adapterContext.getString(R.string.llegada_label, route.getArrivalTime()));

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(adapterContext instanceof RoutesView){
                    RoutesView activity = (RoutesView) adapterContext;
                    activity.itemSelecion(data.get(position));
                }
            }
        });
    }

    class RoutesAdapterViewHolder extends RecyclerView.ViewHolder{
        CardView container;
        TextView lblID;
        TextView lblMarca;
        TextView lblOrigen;
        TextView lblDestino;
        TextView lblSalida;
        TextView lblLlegada;

        public RoutesAdapterViewHolder(View view){
            super(view);
            container = view.findViewById(R.id.container);
            lblID = view.findViewById(R.id.lblID);
            lblMarca = view.findViewById(R.id.lblMarca);
            lblOrigen = view.findViewById(R.id.lblOrigen);
            lblDestino = view.findViewById(R.id.lblDestino);
            lblSalida = view.findViewById(R.id.lblSalida);
            lblLlegada = view.findViewById(R.id.lblLlegada);
        }
    }
}
